﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MonitorService
{
    class TextLogger
    {
        Stream stream;
        StreamWriter writer;
        string fileName;
        public TextLogger(string fileName)
        {
            this.fileName = fileName;
            stream = File.Open(fileName, FileMode.Append, FileAccess.Write, FileShare.Read);
            writer = new StreamWriter(stream);
        }
        public void WriteToFile(string text)
        {
            try
            {
                writer.WriteLine(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "," + text);
                writer.Flush();
            }

            catch
            { };
        }

        public void Close()
        {
            writer.Flush(); stream.Flush();
            writer.Close(); stream.Close();
            writer = null; stream = null;
        }
    }
}
