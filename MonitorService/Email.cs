﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;

namespace MonitorService
{
    public class Email
    {
		string Server;
		string From;
		string Username = null, Password = null;
		int Port = 25;
		bool UseSSL = false;
		SmtpClient SMTPHost = null;
		//MailMessage LastSentMessage;
		//DateTime LastSendMessageTime = DateTime.MinValue;
		//public bool SuppressDuplicateEmails = false;
		//public int SuppressDuplicateEmailsTimeOut = 180; // In Minutes
			
		public Email()
		{
			Server = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPServer");
			From = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPFrom");
			this.NewInstance(Server, From);
		}

		public void NewInstance(string server, string from)
        {
            this.Server = server;
            this.From = from;
            Port = int.TryParse(System.Configuration.ConfigurationManager.AppSettings["SMTPPort"], out Port) ? Port : 25;
            UseSSL = bool.TryParse(System.Configuration.ConfigurationManager.AppSettings["SMTPUseSSL"], out UseSSL) ? UseSSL : false;
            SMTPHost = new SmtpClient(this.Server, Port);
            SMTPHost.EnableSsl = UseSSL;
            SMTPHost.DeliveryMethod = SmtpDeliveryMethod.Network;
            //string SMTPUser = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPUser");
            Username = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPUsername");
            Password = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPPassword");
            if (string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(Password))
                SMTPHost.UseDefaultCredentials = true; 
            else
            {
                SMTPHost.UseDefaultCredentials = false;
                SMTPHost.Credentials = new NetworkCredential(Username, Password); 
            }
        }
        public string SendMail(string recipient, string cc, string subject, string body, string attachment, bool isBodyHtml = false, bool isTaskEmail = false)
        {
            MailMessage msg = new MailMessage();
            try
            {
                if (string.IsNullOrWhiteSpace(subject))
                {
                    subject = "[elluminate®]";
                }
                else if (!subject.StartsWith("[elluminate®]"))
                {
                    subject = "[elluminate®] " + subject;
                }
                if (string.IsNullOrEmpty(Server))
                {
                    throw new Exception("SMTP Server has not been defined. Please contact your Network Administrator");
                }
                msg.From = new MailAddress(this.From);
                if (isTaskEmail)
                {
                    msg.ReplyToList.Add("support@eclinicalsol.com");
                }
                msg.Subject = subject;
                msg.To.Add(recipient);
                if (!string.IsNullOrWhiteSpace(cc))
                {
                    msg.CC.Add(cc);
                }
                msg.Body = body;
                msg.IsBodyHtml = isBodyHtml;
                if (!string.IsNullOrEmpty(attachment)) msg.Attachments.Add(new Attachment(attachment));
                this.SMTPHost.Send(msg);
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                if (ex.InnerException != null)
                {
                    if (ex.InnerException.Message != "" && ex.InnerException.Message != ex.Message)
                    {
                        errMsg += "\r\n" + ex.InnerException.Message;
                    }
                }
                return errMsg;
            }
            finally { msg.Dispose(); msg = null; }

            return string.Empty;
        }
    }
}
