﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace MonitorService
{
    public partial class MonitorService : ServiceBase
    {
        private readonly Timer fileWatcherTimer;
        private TimeSpan fileWatcherTimerInterval;
        private int fileCount = 0;
        static TextLogger Logger = null;
        static TextLogger errLogger = null;
        public MonitorService()
        {
            InitializeComponent();
            fileWatcherTimer = new Timer(FileWatcherTimerCallback, this, Timeout.Infinite, Timeout.Infinite);
            //StartServices();
        }

        protected override void OnStart(string[] args)
        {
            int fileWatcherTimerIntervalMints = int.TryParse(System.Configuration.ConfigurationManager.AppSettings["FileWatcherTimerIntervalMints"], out fileWatcherTimerIntervalMints) ? fileWatcherTimerIntervalMints : 5;
            fileWatcherTimerInterval = new TimeSpan(0, fileWatcherTimerIntervalMints, 0);
            fileWatcherTimer.Change(new TimeSpan(0), fileWatcherTimerInterval);
        }

        protected override void OnStop()
        {
            fileWatcherTimer.Change(Timeout.Infinite, Timeout.Infinite);
        }
        protected override void OnContinue()
        {
            fileWatcherTimer.Change(new TimeSpan(0), fileWatcherTimerInterval);
        }
        /// <summary>
        /// Handles the Elapsed event of the fileWatcherTimer control.
        /// </summary>
        private void FileWatcherTimerCallback(object state)
        {
            StartServices();
        }
        public void StartServices()
        {
            try
            {
                //Set the timer to not send any events while we're busy doing this...(disable the timer)
                fileWatcherTimer.Change(Timeout.Infinite, Timeout.Infinite);

                errLogger = GetErrorLogger();

                int minsToCheckOldFiles = int.TryParse(System.Configuration.ConfigurationManager.AppSettings["minsToCheckOldFiles"], out minsToCheckOldFiles) ? minsToCheckOldFiles : 5;
                DateTime oldestFileTime = DateTime.Now.AddMinutes(-1 * minsToCheckOldFiles);

                //string folderPathtoCheck = System.Configuration.ConfigurationManager.AppSettings["FoldersToCheck"];
                string[] folderstoCheck = ConfigurationManager.AppSettings["FoldersToCheck"].Split(',');

                string csvpath = ConfigurationManager.AppSettings["oldFilesSent"];
                string[,] arColl = new string[,] { };
                arColl = LoadCSV(csvpath);
                List<string> listFilePath = new List<string>();

                foreach (string folderPath in folderstoCheck)
                {
                    listFilePath.AddRange(DirSearch(folderPath));
                }
                
                //List<string> listFilePath = DirSearch(folderPathtoCheck);
                Dictionary<string, string> listOldFiles = new Dictionary<string, string>();

                foreach (string filePath in listFilePath)
                {
                    FileInfo fi = new FileInfo(filePath);
                    if (fi.LastWriteTime < oldestFileTime)
                    {
                        // wrap this in a try/catch and swallow exception because sometimes this fails when file is in use...
                        try
                        {
                            fileCount++;
                            listOldFiles.Add(System.IO.Path.GetFileName(filePath), filePath);
                        }
                        catch (Exception)
                        { }
                    }

                }

                if (fileCount > 0)
                {
                    Email email = new Email();
                    StringBuilder body = new StringBuilder();
                    bool flag = false;
                    string textBody = string.Empty;
                    string txtColumnHeaders = string.Empty;
                    int count = 0;
                    textBody = "<table border=" + 1 + " cellpadding=" + 0 + " cellspacing=" + 0 + " width = " + 400 + ">";
                    textBody += "<tr bgcolor='#4da6ff'>";
                    DataTable dt = new DataTable();
                    dt.Clear();
                    string[] fileItems = ConfigurationManager.AppSettings["fileItems"].Split(',');

                    foreach (string str in fileItems)
                    {
                        dt.Columns.Add(str);
                        textBody += "<td><b>" + str + "</b></td>";
                    }
                    textBody += "</tr>";
                    foreach (KeyValuePair<string, string> entry in listOldFiles)
                    {
                        flag = false;
                        if (arColl != null && arColl.Length != 0)
                        {
                            for (int j = 0; j < arColl.Length / 2; j++)
                            {
                                string dateLogged = arColl[j, 0];
                                string sourceFile = arColl[j, 1];
                                if (entry.Value == sourceFile)
                                {
                                    flag = true;
                                    break;
                                }
                            }
                        }

                        if (flag == false)
                        {
                            count++;
                            Logger = new TextLogger(csvpath);
                            Logger.WriteToFile(entry.Value);
                            // wrap this in a try/catch and swallow exception because sometimes this fails when file is in use...
                            try
                            {
                                XmlTextReader xtr = new XmlTextReader(entry.Value);
                                string dbServer = string.Empty, OutputSchemaName = string.Empty;
                                DataRow row = dt.NewRow();
                                row["FilePath"] = entry.Value;

                                while (xtr.Read())
                                {
                                    foreach (string str in fileItems)
                                    {
                                        if (xtr.NodeType == XmlNodeType.Element && xtr.Name == str)
                                        {
                                            dbServer = xtr.ReadElementString();
                                            row[str] = dbServer;
                                            break;
                                        }

                                    }

                                }
                                dt.Rows.Add(row);
                                
                            }
                            catch (Exception)
                            { }
                            finally
                            {
                                if (Logger != null)
                                {
                                    Logger.Close();
                                    Logger = null;
                                }
                                
                            }
                            
                        }

                    }

                    foreach (DataRow row in dt.Rows)
                    {
                        textBody += "<tr>";
                        foreach (DataColumn column in dt.Columns)
                        {
                            if (row[column] != null) // This will check the null values also (if you want to check).
                            {
                                textBody += "<td>" + row[column].ToString() + "</td>";
                            }
                            else
                            {
                                textBody += "<td></td>";
                            }
                        }
                        textBody += "</tr>";
                    }
                    textBody += "</table>";
                    if (count > 0)
                    {

                        email.SendMail(System.Configuration.ConfigurationManager.AppSettings["ToEmail"], "", $"Old files found are {count}", $"{textBody}", "", true);
                    }
                }
                listOldFiles.Clear();
                return;
            }
            catch (Exception ex)
            {
                errLogger.WriteToFile(ex.ToString());
                errLogger.Close();
                //throw;
            }
            finally
            {
                //Re-enable the timer
                fileWatcherTimer.Change(fileWatcherTimerInterval, fileWatcherTimerInterval);

            }
        }
        public static List<String> DirSearch(string sDir)
        {

            List<String> files = new List<String>();
            try
            {
                foreach (string f in Directory.GetFiles(sDir))
                {
                    files.Add(f);
                }
                foreach (string d in Directory.GetDirectories(sDir))
                {
                    files.AddRange(DirSearch(d));
                }
            }
            catch (System.Exception ex)
            {
                //Logger.Info(ex, "DirSearch()");
                //throw;
            }

            return files;
        }
        public static string[,] LoadCSV(string filename)
        {
            string[,] values = null;
            try
            {

                // Get the file's text.
                string whole_file = System.IO.File.ReadAllText(filename);

                // Split into lines.
                whole_file = whole_file.Replace('\n', '\r');
                string[] lines = whole_file.Split(new char[] { '\r' },
                    StringSplitOptions.RemoveEmptyEntries);

                // See how many rows and columns there are.
                int num_rows = lines.Length;
                int num_cols = lines[0].Split(',').Length;

                // Allocate the data array.
                values = new string[num_rows, num_cols];

                // Load the array.
                for (int r = 0; r < num_rows; r++)
                {
                    string[] line_r = lines[r].Split(',');
                    for (int c = 0; c < num_cols; c++)
                    {
                        values[r, c] = line_r[c];
                    }
                }


            }
            catch (Exception ex)
            {
                //Logger.Info(ex, "LoadCSV()");
            }

            // Return the values.
            return values;
        }
        static TextLogger GetErrorLogger()
        {
            string Startup_Path = AppDomain.CurrentDomain.BaseDirectory;
            string Log_Path = System.IO.Path.Combine(Startup_Path, "Logs");

            if (!System.IO.Directory.Exists(Log_Path))
                System.IO.Directory.CreateDirectory(Log_Path);

            return new TextLogger(Log_Path + "\\Err_" + DateTime.Now.ToString("yyyyMMdd") + ".log");
        }
    }
}
